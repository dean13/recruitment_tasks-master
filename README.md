SimplyGoLive recruitment tasks
==============================

Hi, nice to have you here!
--------------------------

In this package you can find our recruitment tasks. Both of them should be possible to implement in JavaScript with relative ease. Their role is to check your knowledge of JavaScript and general computer programming principles.

You should return your solutions withing 5 work days time, packed into ZIP archive.

Enjoy! And we hope we’ll see you soon!
