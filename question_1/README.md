Compute to 100
==============

Given the digits 1234567890, build an expression by inserting "+" or "-" anywhere BETWEEN the digits so that it results to 100. 
Your answer should be a Node.js script that return all possible combinations. Example: 1 + 23 - 4 + 5 + 6 + 78 - 9 + 0 = 100.