/*

Question 1

*/

const INPUT = "1234567890";

// *** YOUR CODE GOES HERE

function computeTo100() {

	var possibleCompute = [];

	var findSequences = function(str, total) {
    var recurse = function(string, remaining, result) {
        if(string == '') {
            if(remaining == 0) {
				possibleCompute.push(result.substr(1, result.length));
            }
            return;
        }
        var i, tmp;
        for(i = 1; i <= string.length; i++) {
            tmp = parseInt(string.substr(0, i));
            recurse(string.substr(i), remaining - tmp, result + '+' + tmp);
            recurse(string.substr(i), remaining + tmp, result + '-' + tmp);
        }
    };
    recurse(str, total, '');
};

	findSequences(INPUT, 100);

	return possibleCompute;
}

// *** END OF YOUR CODE

console.log(computeTo100()); // should return all possible solutions