Even and odd
============

Given a variable length array of integers, partition them such that the even integers precede the odd integers in the array. 
Take special care of the complexity of your algorythm - best answers should operate in-place and scale linearly in time with respect to the size of the array.