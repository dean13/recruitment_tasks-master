/*

Question 2

*/

// *** YOUR CODE GOES HERE

function evenOdd(table) {

	table.sort(function (a, b) {
		return a % 2 - b % 2 || a - b;
	});

	return table;
}

// *** END OF YOUR CODE

console.log(evenOdd([1, 4, 4, 3, 5])); // should return: [4, 4, 1, 3, 5]
console.log(evenOdd([1, 1, 1, 1])); // should return: [1, 1, 1, 1]
console.log(evenOdd([5, 7, 8, 10])); // should return: [8, 10, 5, 7]